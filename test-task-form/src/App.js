import "./App.css";
import HelpForm from "./components/Form";

function App() {
  return (
    <div className="App">
      <HelpForm />
    </div>
  );
}

export default App;
