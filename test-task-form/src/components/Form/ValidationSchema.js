import * as Yup from "yup";

const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, "Слишком короткое имя")
    .max(50, "Слишком длинное имя")
    .required("Это поле обязательно"),
  lastName: Yup.string()
    .min(2, "Слишком короткая фамилия")
    .max(50, "Слишком длинная фамилия")
    .required("Это поле обязательно"),
  companyName: Yup.string()
    .min(2, "Слишком короткое название")
    .max(50, "Слишком длинное название")
    .required("Это поле обязательно"),
  email: Yup.string()
    .email("Некорректный адрес почты")
    .required("Это поле обязательно"),
  phone: Yup.number().typeError("Некорректный номер телефона"),
  country: Yup.string().required("Это поле обязательно"),
  city: Yup.string().required("Это поле обязательно"),
  district: Yup.string().required("Это поле обязательно"),
  address: Yup.string().required("Это поле обязательно"),
  postIndex: Yup.number()
    .typeError("Введите пожалуйста число")
    .required("Это поле обязательно"),
  cardNumber: Yup.number()
    .typeError("Введите пожалуйста номер")
    .required("Это поле обязательно"),
  CVV: Yup.number().typeError("Введите пожалуйста номер").required(),
  expirationDate: Yup.string().required("Это поле обязательно"),
});

export default validationSchema;
