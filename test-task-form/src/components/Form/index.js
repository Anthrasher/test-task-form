import { Formik, Form } from "formik";
import likeIcon from "../../icons/like.png";
import HelpTypes from "../HelpTypes";
import CustomField from "../CustomField";
import validationSchema from "./ValidationSchema";
import "./Form.scss";

function HelpForm() {
  return (
    <div className="form">
      <p className="form__title">Заповніть форму</p>
      <div className="form__type-button">
        <button className="form__type form__type--active">Фіз. особа</button>
        <button className="form__type ">Юр. особа</button>
      </div>
      <div className="form__container">
        <Formik
          onSubmit={(values, setSubmitting) => {
            console.log(values);
            setSubmitting(false);
          }}
          validationSchema={validationSchema}
          initialValues={{
            firstName: "",
            lastName: "",
            companyName: "",
            email: "",
            phone: "",
            country: "",
            city: "",
            district: "",
            address: "",
            postIndex: "",
            cardNumber: "",
            expirationDate: "",
            CVV: "",
          }}
        >
          {({ errors, touched }) => (
            <Form>
              <div className="form__fields-container">
                <div className="form__column-container">
                  <div className="form__card-data--small-inputs">
                    <CustomField
                      name="firstName"
                      label="Ім'я"
                      errors={errors}
                      touched={touched}
                    />
                    <CustomField
                      name="lastName"
                      label="Фамілія"
                      errors={errors}
                      touched={touched}
                    />
                  </div>
                  <CustomField
                    name="companyName"
                    label="Назва компанії, організації"
                    errors={errors}
                    touched={touched}
                  />
                  <CustomField
                    name="email"
                    label="Email-адрес"
                    errors={errors}
                    touched={touched}
                  />
                  <CustomField
                    name="phone"
                    label="Номер телефону"
                    errors={errors}
                    touched={touched}
                  />
                </div>
                <div className="form__column-container">
                  <CustomField
                    name="country"
                    label="Країна"
                    errors={errors}
                    touched={touched}
                  />
                  <div className="form__card-data--small-inputs">
                    <CustomField
                      name="city"
                      label="Місто"
                      errors={errors}
                      touched={touched}
                    />
                    <CustomField
                      name="district"
                      label="Штат, район"
                      errors={errors}
                      touched={touched}
                    />
                  </div>
                  <CustomField
                    name="address"
                    label="Адреса"
                    errors={errors}
                    touched={touched}
                  />
                  <CustomField
                    name="postIndex"
                    label="Поштовий індекс"
                    errors={errors}
                    touched={touched}
                  />
                </div>
              </div>

              <HelpTypes />
              <div className="form__payment">
                <div className="form__payment-methods-container">
                  <p className="form__payment-title">Спосіб оплати</p>
                  <ul className="form__payment-methods">
                    <li className="form__payment-method" id="visa">
                      <img
                        alt=""
                        src={likeIcon}
                        className="form__payment-method-img"
                      ></img>
                      <p className="form__payment-method-title">
                        Карта Visa/Mastercard
                      </p>
                    </li>
                    <li
                      className="form__payment-method form__payment-method--active"
                      id="privat"
                    >
                      <img
                        alt=""
                        src={likeIcon}
                        className="form__payment-method-img"
                      ></img>
                      <p className="form__payment-method-title">Приват24</p>
                    </li>
                    <li className="form__payment-method" id="terminal">
                      <img
                        alt=""
                        src={likeIcon}
                        className="form__payment-method-img"
                      ></img>
                      <p className="form__payment-method-title">
                        Термінали України
                      </p>
                    </li>
                    <li className="form__payment-method" id="webmoney">
                      <img
                        alt=""
                        src={likeIcon}
                        className="form__payment-method-img"
                      ></img>
                      <p className="form__payment-method-title">WebMoney</p>
                    </li>
                    <li className="form__payment-method" id="paypal">
                      <img
                        alt=""
                        src={likeIcon}
                        className="form__payment-method-img"
                      ></img>
                      <p className="form__payment-method-title">PayPal</p>
                    </li>
                  </ul>
                </div>
                <div className="form__card-data-container">
                  <p className="form__payment-title">Введіть наступні дані</p>
                  <div className="form__card-data">
                    <CustomField
                      name="cardNumber"
                      label="Номер карти"
                      background="white"
                      color="white"
                      errors={errors}
                      touched={touched}
                    />
                    <div className="form__card-data--small-inputs">
                      <CustomField
                        name="expirationDate"
                        label="Термін дії"
                        background="white"
                        color="white"
                        errors={errors}
                        touched={touched}
                      />
                      <CustomField
                        name="CVV"
                        label="CVC/CVV"
                        background="white"
                        color="white"
                        errors={errors}
                        touched={touched}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" className="submit-button">
                Допомогти
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

export default HelpForm;
