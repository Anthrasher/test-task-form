import "./HelpTypes.scss";
import likeIcon from "../../icons/like.png";

function HelpTypes() {
  return (
    <div>
      <p className="form__title">Види допомоги</p>
      <p className="form__hint">Ви можете змінити вид допомоги</p>
      <ul className="form__help-types">
        <li className="form__help-type">
          <div className="form__help-type-img">
            <img className="form__icon" alt="" src={likeIcon} />
          </div>
          <p className="form__help-type-title">Зробити</p>
        </li>
        <li className="form__help-type form__help-type--active">
          <div className=" form__help-type-img form__help-type-img--active">
            <img className="form__icon" src={likeIcon} alt="" />
          </div>
          <p className="form__help-type-title--active form__help-type-title">
            Фінансова допомога
          </p>
        </li>
        <li className="form__help-type">
          <div className="form__help-type-img">
            <img className="form__icon" alt="" src={likeIcon} />
          </div>
          <p className="form__help-type-title">Матеріальна допомога</p>
        </li>
        <li className="form__help-type">
          <div className="form__help-type-img">
            <img className="form__icon" alt="" src={likeIcon} />
          </div>
          <p className="form__help-type-title">Волонтерство</p>
        </li>
      </ul>
    </div>
  );
}

export default HelpTypes;
