import { Field } from "formik";
import "./CustomField.scss";

function CustomField({
  name,
  label,
  type,
  background,
  color,
  errors,
  touched,
}) {
  return (
    <div className="field__container">
      <label htmlFor={name} className="field__label" color={color}>
        {label}
      </label>
      <Field
        name={name}
        type={type || "text"}
        id={name}
        className="field__input"
        background={background}
      />
      {errors[name] && touched[name] && (
        <p className="field__error">{errors[name]}</p>
      )}
    </div>
  );
}

export default CustomField;
